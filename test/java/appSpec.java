package app;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Usability unit tests")
@WebMvcTest
public class appSpec {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_outputCarID_whenValidRequest()
            throws Exception {
        Car car = new Car(new CarId(1), new Vin("1HGBH41JXMN109186"), new Model("Honda CRV"));
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("1")));
    }

}
