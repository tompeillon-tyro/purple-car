package app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class unit {

    @Test
    public void should_fail_whenCarIdIsNegative() {
        Assertions.assertThrows(RuntimeException.class, () -> new CarId(-1));
    }

    @Test
    public void should_fail_whenVIN_IsNot17Characters() {
        Assertions.assertThrows(RuntimeException.class, () -> new Vin("1"));
    }


    @Test
    public void should_fail_whenVIN_IsNotAlphaNumeric() {
        Assertions.assertThrows(RuntimeException.class, () -> new Vin("1234567890123456?"));
    }

    @Test
    public void should_fail_whenVIN_IsNotInCorrectFormat() {

        Assertions.assertThrows(RuntimeException.class, () -> new Vin("A234567890123456?"));
    }

    @Test
    public void should_fail_whenModel_IsEmpty() {
        Assertions.assertThrows(RuntimeException.class, () -> new Model(""));
    }

    @Test
    public void should_fail_whenModel_IsOver20Chars() {
        Assertions.assertThrows(RuntimeException.class, () -> new Model("123456789012345678901"));
    }

    @Test
    public void should_fail_whenModel_IsNoAlphaNumeric() {
        Assertions.assertThrows(RuntimeException.class, () -> new Model("#?"));
    }

}
