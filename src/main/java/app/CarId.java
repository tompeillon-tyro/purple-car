package app;

public final class CarId {
    private Integer value;

    public CarId(Integer value) {
        if (value == null || value < 0) {
            throw new RuntimeException("Invalid Car Id");
        }

        this.value = value;
    }



    public Integer getCarId() {
        return value;
    }
}
