package app;

public final class Vin {
    private String value;

    public Vin(String value) {
        // Check not null
        if (value == null) {
            throw new RuntimeException("Invalid VIN");
        }
        // Check correct length
        if (value.length() != 17) {
            throw new RuntimeException("Invalid VIN");
        }
        // Check alphanumeric
        if (!value.matches("[A-Z0-9]+")) {
            throw new RuntimeException("Invalid VIN");
        }
        // Check correct format - TODO
        if (!value.matches("[0-9][A-Z][A-Z][A-Z][A-Z][0-9][0-9][A-Z][A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]")) {
            throw new RuntimeException("Invalid VIN");
        }

        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
