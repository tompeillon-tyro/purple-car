package app;

public final class Car {

    private CarId carID;
    private Vin vin;
    private Model model;
    //private String plateNo;
    //private File photo;

    public Car(CarId carID, Vin vin, Model model) {
        this.carID = carID;
        this.vin = vin;
        this.model = model;
    }

    public CarId getcarID() {
        return this.carID;
    }

    public Vin getvin() {
        return this.vin;
    }

    public Model getmodel() {
        return this.model;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " " + this.vin + " " + this.model;
    }
}
