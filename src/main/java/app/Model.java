package app;

public final class Model {

    private String value;


    public Model(String value) {
        // Check not null
        if (value == null) {
            throw new RuntimeException("Invalid Model");
        }
        // Sanity check length
        if (value.length() > 20 || value.length() == 0) {
            throw new RuntimeException("Invalid Model");
        }
        // Check alphanumeric
        if (!value.matches("[a-zA-Z0-9 ]+")) {
            throw new RuntimeException("Invalid Model");
        }

        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
